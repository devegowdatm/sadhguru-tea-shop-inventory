import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AddItem from "./AddItem";
import Item from "./Item";


class App extends Component {

	render() {
		return (
			<Router>
				<Switch>
					<Route path="/" exact component={AddItem} />
					<Route path="/item/:id" exact component={Item} />
				</Switch>
			</Router>
		);
	}

}

export default App;