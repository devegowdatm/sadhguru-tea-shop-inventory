import React from "react";
import { Link } from "react-router-dom";


const ItemList = (props) => {
	return props.items.map((item) => {
		const thumbnail = (Object.entries(item.thumbnail).length && item.thumbnail.unique_name) ?
							`static/media/${item.thumbnail.unique_name}`							
							:
							'/static/media/default-icon.jpeg';

		return (
			<div className="col-xs-6 col-md-4 col-lg-3 item"  key={item.id}>
				<button className="btn" onClick={() => props.delete(item.id)}>x</button>
				<Link to={`/item/${item.id}`} >
					<img src={thumbnail} title={item.description} alt="thumbnail"/>
					<div className="desc name"><span>Name: </span>{item.name}</div>
					<div className="desc price"><span>price: </span>{item.price} $</div>
				</Link>
			</div>
		)
	})
};

export default ItemList;
