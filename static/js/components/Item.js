import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isloading: false,
      id: +this.props.match.params.id, //Unary + operator to convert string to int.
      thumbnail: {},
      name: '',
      error: false
    };
  }
 
  componentDidMount() {
    axios.get(`/api/file-manager/${this.state.id}`)
      .then(response => {
        let {name, price, description, thumbnail={}} = response.data.item_data;
        thumbnail.path = `/static/media/` + (thumbnail.unique_name || `default-icon.jpeg`);
        if (!thumbnail.name) thumbnail.name = 'thumbnail';
        this.setState({
          name: name,
          price: price,
          description: description,
          thumbnail: thumbnail
        })
    })
    .catch(response => {
      this.setState({
        error: true
      })
    });
  }

  render() {
    return (
        <>
          <Link to={`/`} className="back-link">
          <button className="btn back-btn" >&larr;</button>
          </Link>
          <h1>Item Details</h1>
          {!this.state.error ?
            <div className="Item-block container">
              <div className="row">
                <div className="thumbnail-block col-lg-4">
                  <figure>
                    <img src={this.state.thumbnail.path} alt={this.state.thumbnail.name}/>
                    <figcaption>{this.state.name}</figcaption>
                  </figure>
                  <p className="item-price"><span>Price: </span>{this.state.price} $</p>
                </div>
                <p className="item-description col-lg-8"><span>Description: </span> {this.state.description}</p>
                </div>
            </div>
            : <div className="data-error">ITEM NOT AVAILABLE</div>
          }
      </>
    );
  }
}

export default Item;
