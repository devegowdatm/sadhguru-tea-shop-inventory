import React, { Component } from "react";
import axios from 'axios';
import $ from 'jquery';

import ItemList from './ItemList';
import '../../css/index.css';

class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      items: []
    };
  }

  addItem = (e) => {
    e.preventDefault();
    document.getElementById('error-message').style.display = 'none';
    document.getElementById('success-message').style.display = 'none';
    if (!this.validateForm()) return;
    let itemData = new FormData();
    itemData.set('name', this._name.value);
    itemData.set('price', this._price.value);
    itemData.set('description', this._description.value || '');
    if (this._file.files.length) {
      itemData.set('thumbnail', this._file.files[0]);
    }
    axios({
      method: 'POST',
      url: '/api/file-manager/',
      data: itemData,
      headers: {
        "Content-Type": 'multipart/form-data',
        "X-CSRFToken": $("meta[name=csrf-token]").attr("content")
      }
    })
    .then( response => {
      //handle success
      document.getElementById('success-message').style.display = 'block';
      this.setState({
        items: [...this.state.items, response.data.item_data]
      }, () => {
        setTimeout(() => {
          document.getElementById('success-message').style.display = 'none'; 
            
        }, 3000)
      })
      document.getElementById('item-form').reset()
    })
    .catch( response => {
      this.setState({
        errorMessage: 'Failed to submit'
      })
      document.getElementById('error-message').style.display = 'block';
    });
  }

  validateForm = () => {
    let errorMessage = '',
      isValid = true;
    if(!this._name.value.trim() || !this._description.value.trim()) {
      errorMessage = 'Name/Description are mandatory';
      isValid = false;
      document.getElementById('error-message').style.display = 'block';
    }
    else if (
      this._file.files.length &&
      !(/\.(jfif|jpg|jpeg|png|gif)$/i).test(this._file.files[0].name)
    ) {
      isValid = false;
      errorMessage = 'Only Image file is allowed with the format jfif, jpg, jpeg, png, gif ';
    }
    else if(isNaN(this._price.value)) {
      isValid = false;
      errorMessage = 'Price should be in Numbers';
    }
    if (!isValid) {
      this.setState({
        errorMessage: errorMessage
      })
      document.getElementById('error-message').style.display = 'block';
    }
    return isValid
  }

  deleteItem = (id) => {
    axios.delete(`/api/file-manager/${id}`,
      {
        headers: {"X-CSRFToken": $("meta[name=csrf-token]").attr("content")}
    })
    .then(response => {
        this.setState({
            items: this.state.items.filter(item => { return item.id != id})
        })
    })
    .catch(function (response) {
        console.log(response);
    });
  }

  componentDidMount() {
    axios.get('/api/file-manager/')
    .then(response => {
      this.setState({
        items: response.data.all_items
      });
    })
    .catch(response => {
      console.log(response)
    });
  }

  render() {
    return (
      <>
        <h1>Inventory Items</h1>
        <div className="form-container">
          <div id="form-main">
            <div id="form-div">
              <form className="item-form" id="item-form" onSubmit={(e)=> this.addItem(e)}>
                <p className="name">
                  <input name="name" type="text" className="item-input"
                    required placeholder="Name" id="name" ref={(name) => this._name = name} />
                </p>
                <p className="price">
                  <input name="number" type="number" required className="item-input"
                  id="price" placeholder="Price In $" ref={(price)=> this._price = price} />
                </p>
                <p className="text">
                  <textarea name="message" className="item-input" required
                    id="description" placeholder="Description" ref={(description)=> this._description = description}></textarea>
                </p>
                <p className="file">
                  <input name="image" type="file" accept="image/*"
                    id="file" className="item-input" ref={(_file)=> this._file = _file} />
                </p>
                <div className="submit">
                  <button type="submit" className="button-submit">SUBMIT</button>
                </div>
              </form>
              <div id="error-message">
                <h4>{this.state.errorMessage}</h4>
              </div>
              <div id="success-message">
                <h4>Item was Saved Successfully.</h4> </div>
            </div>
          </div>
        </div>
        {this.state.items.length ?
          <div className="items-list container">
            <div className="row">
              <ItemList items={this.state.items} delete={(id) => this.deleteItem(id)}/>
          </div>
          </div> : null
        }
      </>
    );
  }
}

export default AddItem;
