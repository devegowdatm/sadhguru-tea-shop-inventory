from flask import (
    Blueprint,
    render_template
)

main = Blueprint("main", __name__)


@main.route("/")
@main.route("/item/<id>")
def item(id=None):
    return render_template('index.html')
