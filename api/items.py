import os

from uuid import uuid4
from flask import current_app
from flask_restful import Resource, abort, reqparse, fields, marshal
from werkzeug.datastructures import FileStorage

from models.items import db, Item, Thumbnail

thumbnail_api_fields = {
    'name': fields.String,
    'unique_name': fields.String
}

item_api_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'price': fields.String,
    'thumbnail': fields.Nested(thumbnail_api_fields, default={})
}

parser = reqparse.RequestParser()
parser.add_argument(
    'name', type=unicode, location='form', help='Name required')
parser.add_argument('description', type=unicode, location='form')
parser.add_argument(
    'price', type=int, location='form', help='Price required')
parser.add_argument(
    'thumbnail', type=FileStorage, location='files', help='File required')


class ItemsAPI(Resource):
    """Items API"""
    def get(self, id=None):
        if id:
            item = Item.query.get_or_404(id, description="ITEM NOT AVAILABLE")
            return marshal(item, item_api_fields, envelope='item_data')

        items = Item.query.all()
        return marshal(items, item_api_fields, envelope="all_items")

    def post(self):
        post_data = parser.parse_args()
        try:
            name = post_data.get('name')
            description = post_data.get('description')
            price = post_data.get('price')
            if not (name or price or description):
                abort(412, description='NAME/PRICE/Description DATA MISSIG')

            item = Item()
            item.name = name
            item.description = description
            item.price = price
            db.session.add(item)
            thumbnail = post_data.get('thumbnail')
            if thumbnail:
                if not thumbnail.mimetype.startswith('image'):
                    abort(415, description="FILE FORMAT NOT ALLOWED")
                name, ext = os.path.splitext(thumbnail.filename)
                # If the file object doesn't have any filename extension.
                if not ext:
                    ext = '.' + thumbnail.mimetype.split('/')[1]

                hex_file_name = str(uuid4()) + ext

                # Create media folder if not exist
                if not os.path.exists(current_app.config.get('MEDIA_FOLDER')):
                    os.makedirs(current_app.config.get('MEDIA_FOLDER'))

                thumbnail.save(
                    os.path.join(
                        current_app.config.get('MEDIA_FOLDER'),
                        hex_file_name)
                )

                _thumbnail = Thumbnail()
                _thumbnail.name = thumbnail.filename
                _thumbnail.unique_name = hex_file_name
                _thumbnail.item = item
                db.session.add(_thumbnail)

            db.session.commit()

        except Exception as e:
            return str(e)

        return marshal(item, item_api_fields, envelope='item_data'), 201

    def delete(self, id):
        item = Item.query.get_or_404(id, description='ITEM NOT Availbele')
        db.session.delete(item)
        item.remove_thumbnail
        db.session.commit()

        return {'status': 'deleted'}, 201
