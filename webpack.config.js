const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  output: {
    path: __dirname + "/static/js/build",
    filename: "[name].pack.js"
  },
  entry: {
    home: __dirname + "/static/js/index.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      
    ]
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(), 
  ]
};